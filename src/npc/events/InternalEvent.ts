export class InternalEvent {
  constructor(public type: string, power: number, pain: number = 0) {}
}