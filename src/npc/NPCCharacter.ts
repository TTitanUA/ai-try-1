import {App} from "../App";
import {INPCState} from "../state/IState";
import {Organism} from "./bodyComponents/Organism";
import {EVENT_TYPE_TICK} from "../game/Time";
import {InternalEvent} from "./events/InternalEvent";

export class NPCCharacter {
  myState: INPCState
  organism: Organism

  constructor(public app: App, public id: string) {
    this.myState = this.app.state.getNpc(id)
    this.organism = new Organism(this)
    this.spawn()
  }

  private spawn() {
    this.app.world.time.subscribe(this.externalInfluence.bind(this))
    console.group("NPCCharacter.ts:13 - spawn");
    console.log(this);
    console.groupEnd();
  }

  public externalInfluence(event: Event) {
    switch (event.type) {
      case EVENT_TYPE_TICK: this.organism.cyclicalChanges(event);
      break;
    }
  }

  public internalInfluence(event: InternalEvent) {
    console.group("NPCCharacter.ts:32 - internalInfluence");
    console.log(event);
    console.log(this.app.state);
    console.groupEnd();
  }
}