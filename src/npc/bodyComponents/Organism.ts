import {NPCCharacter} from "../NPCCharacter";
import {Energy} from "./Energy";

export class Organism {
  private energy: Energy

  constructor(private character: NPCCharacter) {
    this.energy = new Energy(character)
  }

  public cyclicalChanges(event: Event) {
    this.energy.cyclicalChanges(event)
  }
}