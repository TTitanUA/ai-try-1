import {NPCCharacter} from "../NPCCharacter";
import {InternalEvent} from "../events/InternalEvent";

export class Energy {
  private leekInterval: number = 1000

  constructor(private character: NPCCharacter) {}

  public cyclicalChanges(event: Event) {
    if(event.timeStamp % this.leekInterval < this.character.app.world.time.TICK_INTERVAL && this.character.myState.status.energy > 0) {
      this.character.myState.status.energy -= 0.01
    }

    if(this.character.myState.status.energy < this.character.myState.baseStatus.energy * 0.3) {
      this.character.internalInfluence(
        new InternalEvent(
          'tiredness',
          (this.character.myState.baseStatus.energy / (this.character.myState.baseStatus.energy - this.character.myState.status.energy)) * 100
        )
      )
    }
  }
}