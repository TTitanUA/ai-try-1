import { openDB, IDBPDatabase } from 'idb'
import { IState } from './IState'
import {defaultState} from './Default'

const DB = 'game'
const STATE_STORE = 'gameState'

export class StateStorage {
  db: IDBPDatabase<unknown>

  private version: 2

  private async init() {
    this.db = await openDB(DB, this.version, {
      upgrade(db, oldVersion, newVersion, transaction) {
        db.createObjectStore(STATE_STORE);
      },
      blocked() {
        // …
      },
      blocking() {
        // …
      },
      terminated() {
        // …
      },
    });
  }

  public async load(id = '1'): Promise<IState> {
    if(!this.db) {
      await this.init()
    }

    const value = await this.db.get(STATE_STORE, id)

    if(!value) {
      return defaultState
    }

    return value as IState
  }

  public async save(id = '1', state: IState): Promise<void> {
    if(!this.db) {
      await this.init()
    }

    await this.db.put(STATE_STORE, state, id)
  }
}