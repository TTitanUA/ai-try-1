export interface IState {
  npcList: {[key: string]: INPCState}
}

export interface INPCState {
  id: string
  name: string
  firstName: string
  lastName: string
  status: {
    health: number
    energy: number
    hunger: number
  },
  baseStatus: {
    health: number
    energy: number
    hunger: number
  }
}