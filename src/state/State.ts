import { StateStorage } from './StateStorage'
import { IState } from './IState'
import { defaultNpc } from './Default'

export class State {
  storage = new StateStorage()

  data: IState

  public async load() {
    this.data = await this.storage.load('1')
  }

  public getNpc(id: string) {
    if(!this.data.npcList[id]) {
      this.data.npcList[id] = {...defaultNpc, id}
    }

    return this.data.npcList[id]
  }

  public getAllNpc() {
    return this.data.npcList
  }

  public static async make() {
    const state = new State()
    await state.load()
    return state;
  }
}