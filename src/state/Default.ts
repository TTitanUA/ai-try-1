import {INPCState, IState} from './IState'

export const defaultNpc: INPCState = {
  id: '',
  name: '',
  firstName: '',
  lastName: '',
  status: {
    energy: 100,
    health: 100,
    hunger: 0
  },
  baseStatus: {
    energy: 100,
    health: 100,
    hunger: 0
  }
}

const vasyaPetrov: INPCState = {
  id: 'vasyaPetrov',
  name: 'Вася Петров',
  firstName: 'Вася',
  lastName: 'Петров',
  status: {
    energy: 100,
    health: 100,
    hunger: 0
  },
  baseStatus: {
    energy: 100,
    health: 100,
    hunger: 0
  }
}

export const defaultState: IState = {
  npcList: {vasyaPetrov}
}

