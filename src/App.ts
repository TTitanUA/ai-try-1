import { State } from './state/State'
import { World } from "./game/World";

export class App {
  state: State
  world: World



  public static async bootstrap() {
    const app = new App()
    app.state = await State.make()
    app.world = await World.make(app)
    app.world.init()
    // @ts-ignore
    window['app'] = app
  }
}