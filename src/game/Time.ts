export const EVENT_TYPE_TICK = 'tick'

export class Time extends EventTarget {
  TICK_INTERVAL = 100

  constructor() {
    super();
    this.loop()
  }

  public subscribe(cb: (event: Event) => void) {
    return this.addEventListener(EVENT_TYPE_TICK, cb)
  }

  private loop() {
    setInterval(() => {
      this.dispatchEvent(new Event(EVENT_TYPE_TICK))
    }, this.TICK_INTERVAL)
  }
}