import {App} from "../App";
import {NPCCharacter} from "../npc/NPCCharacter";
import {Time} from "./Time";

export class World {

  public npcList: NPCCharacter[] = []
  public time = new Time()

  constructor(private app: App) {}

  public init() {
    this.spawn()
  }

  private spawn() {
    this.npcList = Object.keys(this.app.state.getAllNpc())
      .map((id) => new NPCCharacter(this.app, id))
  }

  public static async make(app: App): Promise<World> {
    return new World(app)
  }
}