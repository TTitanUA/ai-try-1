
module.exports = (arg) => {
  const mode = arg.dev ? 'dev' : 'prod'
  console.log(`🛠️  running ${mode} Mode using ./webpack/webpack.${mode}.js 🛠️`);
  return require(`./webpack/webpack.${mode}.js`);
};